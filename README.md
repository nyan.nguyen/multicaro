# Caro Online Game

Multiplayer game made with [NodeJs](https://nodejs.org/en/), [Socket.io](https://socket.io/) and [p5.js](https://p5js.org/)

## Running Locally

Make sure you have [Node.js](http://nodejs.org/) 

```sh
npm install
npm start
```

Your app should now be running on [localhost:3000](http://localhost:3000/).